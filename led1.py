import time
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BOARD)

GPIO.setup(11, GPIO.OUT) # left reverse
GPIO.setup(13, GPIO.OUT) # left forward
GPIO.setup(16, GPIO.OUT) # right reverse
GPIO.setup(18, GPIO.OUT) # right forward

for i in range(4):
    GPIO.output(13, True)
    GPIO.output(18, True)
    time.sleep(1)
    GPIO.output(13, False)
    GPIO.output(18, False)
    print('on ' + str(i))
    time.sleep(2)
    GPIO.output(11, True)
    GPIO.output(16, True)
    time.sleep(1)
    GPIO.output(11, False)
    GPIO.output(16, False)
    print('off ' + str(i))
    time.sleep(2)

GPIO.cleanup()
