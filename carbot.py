# Imports.
import io
import picamera
import pygame
import pygame.locals
import RPi.GPIO as gpio
import sys
import time

# Build pygame screen
pygame.init()
BLACK = (0, 0, 0)
WIDTH = 640
HEIGHT = 480
windowSurface = pygame.display.set_mode((WIDTH, HEIGHT), 0, 32)

# windowSurface.fill(BLACK)
camera = picamera.PiCamera()
camera.resolution = (640, 480)
camera.crop = (0.0, 0.0, 1.0, 1.0)

x = (windowSurface.get_width() - camera.resolution[0]) / 2
y = (windowSurface.get_height() - camera.resolution[1]) / 2

# Init buffer
rgb = bytearray(camera.resolution[0] * camera.resolution[1] * 3)

# Setup raspberry pi gpio pins
gpio.setwarnings(False)
gpio.setmode(gpio.BOARD)

gpio.setup(11, gpio.OUT)  # left reverse
gpio.setup(13, gpio.OUT)  # left forward
gpio.setup(16, gpio.OUT)  # right reverse
gpio.setup(18, gpio.OUT)  # right forward


# Functions for movement
def forward():
    gpio.output(13, True)
    gpio.output(18, True)
    time.sleep(.01)
    gpio.output(13, False)
    gpio.output(18, False)
    return


def reverse():
    gpio.output(11, True)
    gpio.output(16, True)
    time.sleep(.01)
    gpio.output(11, False)
    gpio.output(16, False)
    return


def left():
    gpio.output(11, True)
    gpio.output(18, True)
    time.sleep(.01)
    gpio.output(11, False)
    gpio.output(18, False)
    return


def right():
    gpio.output(16, True)
    gpio.output(13, True)
    time.sleep(.01)
    gpio.output(16, False)
    gpio.output(13, False)
    return


while True:
    # get all the user events
    for event in pygame.event.get():
        # if user wants to quit
        if event.type == pygame.locals.QUIT:
            # and the game close the window
            camera.close()
            pygame.display.quit()
            gpio.cleanup()
            pygame.quit()
            sys.exit()

    pressed = pygame.key.get_pressed()
    if pressed[pygame.K_d]:
        right()
    if pressed[pygame.K_a]:
        left()
    if pressed[pygame.K_w]:
        forward()
    if pressed[pygame.K_s]:
        reverse()
    if pressed[pygame.K_x]:
        gpio.cleanup()
        pygame.quit()
        sys.exit()

    stream = io.BytesIO()
    camera.capture(stream, use_video_port=True, format='rgb')
    stream.seek(0)
    stream.readinto(rgb)
    stream.close()
    img = pygame.image.frombuffer(rgb[0:(camera.resolution[0] *
                                  camera.resolution[1] * 3)],
                                  camera.resolution, 'RGB')
    windowSurface.fill(0)
    if img:
        windowSurface.blit(img, (x, y))

    pygame.display.update()
